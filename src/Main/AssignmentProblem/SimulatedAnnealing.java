package Main.AssignmentProblem;

import Main.ReadFile;
import Main.Tools;

import java.util.ArrayList;
import java.util.Collections;

public class SimulatedAnnealing {

    final private static double alpha = 0.5;
    private static double temperature = 500;
    final private static int MAX_ITERATIONS = 670;
    final private static int N = 500;

    private static double coolDown(final double temp) {
        return temp * alpha;
    }

    private static void swap(final ArrayList<Integer> assignedTasks) {
        final int size = assignedTasks.size() - 1;
        final int p1 = Tools.getRandomInt(0, size);
        final int p2 = Tools.getRandomInt(0, size);
        final int tmp = assignedTasks.get(p1);
        assignedTasks.set(p1, assignedTasks.get(p2));
        assignedTasks.set(p2, tmp);
    }

    private static void swap2(final ArrayList<Integer> assignedTasks) {
        final int size = assignedTasks.size();
        int sectionStart, sectionEnd;
        do {
            sectionStart = Tools.getRandomInt(0, size - 1);
            sectionEnd = Tools.getRandomInt(0, size - 1);
        } while (sectionEnd < sectionStart);

        int last = sectionEnd;
        for (int i = sectionStart; i < sectionEnd; i++) {
            final int tmp = assignedTasks.get(i);
            assignedTasks.set(i, assignedTasks.get(last));
            assignedTasks.set(last, tmp);
            last--;
        }
    }

    private static int SA(final int[][] tasks) {
        final int size = tasks.length;
        ArrayList<Integer> taskAssignedToWorker = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            taskAssignedToWorker.add(i);
        }
        Collections.shuffle(taskAssignedToWorker);
        int vc = Helper.result(tasks, taskAssignedToWorker);

        for (int i = 0; i < MAX_ITERATIONS; i++) {
            for (int j = 0; j < N; j++) {
                final ArrayList<Integer> vnTasks = (ArrayList<Integer>) taskAssignedToWorker.clone();
                swap2(vnTasks);
                final int vn = Helper.result(tasks, vnTasks);

                if (vn < vc) {
                    vc = vn;
                    taskAssignedToWorker = (ArrayList<Integer>) vnTasks.clone();
                    continue;
                }

                final double rand = Tools.getRandomDouble();
                final double exp = Math.exp(-(vn - vc) / temperature);
                if (rand < exp) {
                    vc = vn;
                }
            }
            temperature = coolDown(temperature);
        }

        return vc;
    }

    public static void main(final String[] args) {
        final ArrayList<String> lines = ReadFile.getDataFromFile("/src/Main/SA/assign500.txt");
        final int workers = Integer.parseInt(lines.get(0).trim().split(" ")[0]);
        final int[][] tasks = Helper.getTasks(workers, lines);
        // int sum = 0;
        for (int i = 0; i < 10; i++) {
            final int sa = SA(tasks);
            System.out.println(sa);
            // System.out.println(i + ". Próba " + sa);
            // sum += sa;
        }

        // System.out.println("Średnia " + sum / 10);

    }
}
