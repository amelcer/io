package Main.TSP;

import java.util.ArrayList;
import java.util.Iterator;

public class Path implements Iterable<Point> {

    ArrayList<Point> path = new ArrayList<>();
    int current = 0;

    public Path() {
    }

    public void printPath() {
        for (final Point p : path) {
            System.out.print(p.getId() + "-->");
        }
        System.out.println();
    }

    public double getPathLength() {
        double length = 0;
        for (int i = 0; i < path.size() - 1; i++) {
            length += path.get(i).getDistanceTo(path.get(i + 1));
        }

        return length;
    }

    public void swap(final int ind1, final int ind2) {
        final int size = path.size();
        if (ind1 < 0 || ind2 < 0 || ind1 >= size || ind2 >= size)
            throw new IndexOutOfBoundsException();

        final Point tmp = path.get(ind1);
        path.set(ind1, path.get(ind2));
        path.set(ind2, tmp);
    }

    public Point getPoint(final int i) {
        return path.get(i);
    }

    public Path copy() {
        final Path newPath = new Path();
        this.path.forEach(p -> newPath.add(p));

        return newPath;
    }

    public void add(final Point p) {
        path.add(p);
    }

    public int size() {
        return path.size();
    }

    public void removeLast() {
        path.remove(path.size() - 1);
    }

    public void returnToFirstCity() {
        final Point first = path.get(0);
        path.add(first);
    }

    @Override
    public Iterator<Point> iterator() {
        return path.iterator();
    }
}
