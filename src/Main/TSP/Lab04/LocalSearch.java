package Main.TSP.Lab04;

import Main.Tools;
import Main.TSP.Path;
import Main.TSP.Point;

public class LocalSearch {

    Point[] points;
    int dimension;

    public LocalSearch(final Point[] points) {
        this.points = points;
        this.dimension = points.length;
    }

    public Path findPath1_1() {
        final Path path = new Path();
        fillPath(path);
        return localSearch_1(path);
    }

    public Path findPath1_2() {
        final Path path = new Path();
        fillPath(path);
        return localSearch_1_fromAll(path);
    }

    public Path findPath1_3(Path randomPath) {
        return localSearch_1(randomPath.copy());
    }

    public Path findPath1_4(Path randomPath) {
        return localSearch_1_fromAll(randomPath.copy());
    }

    public Path findPath1_5(Path bestPath) {
        return localSearch_1(bestPath.copy());
    }

    public Path findPath1_6(Path bestPath) {
        return localSearch_1_fromAll(bestPath.copy());
    }

    public Path findPath2_1() {
        final Path path = new Path();
        fillPath(path);
        return localSearch_2(path);
    }

    public Path findPath2_2() {
        final Path path = new Path();
        fillPath(path);
        return localSearch_2_fromAll(path);
    }

    public Path findPath2_3(Path randomPath) {
        return localSearch_2(randomPath.copy());
    }

    public Path findPath2_4(Path randomPath) {
        return localSearch_2_fromAll(randomPath.copy());
    }

    public Path findPath2_5(Path bestPath) {
        return localSearch_2(bestPath.copy());
    }

    public Path findPath2_6(Path bestPath) {
        return localSearch_2_fromAll(bestPath.copy());
    }

    private Path localSearch_1(final Path startPath) {
        final int minIndex = 0;
        final int maxIndex = startPath.size() - 1;
        Path vc = startPath.copy();
        double vcLength;
        double vnLength;
        boolean local = false;

        do {
            final int p1 = Tools.getRandomInt(minIndex, maxIndex);
            final int p2 = Tools.getRandomInt(minIndex, maxIndex);
            final Path vn = vc.copy();
            vn.swap(p1, p2);
            vcLength = vc.getPathLength();
            vnLength = vn.getPathLength();
            if (vnLength < vcLength)
                vc = vn.copy();
            else
                local = true;

        } while (!local);

        vc.returnToFirstCity();
        return vc;
    }

    private Path localSearch_1_fromAll(final Path startPath) {
        final int maxIndex = startPath.size() - 1;
        Path vc = startPath.copy();
        double vcLength;
        double vnLength;

        for (int i = 0; i < maxIndex; i++) {
            vcLength = vc.getPathLength();
            // .out.println(i + ". Długość VC " + vcLength);
            for (int j = 1; j < maxIndex; j++) {
                final Path vn = vc.copy();
                vn.swap(i, j);
                vnLength = vn.getPathLength();
                // System.out.println("\t" + i + "." + j + " Długość VN " + vnLength);
                if (vnLength < vcLength)
                    vc = vn.copy();
            }
        }

        vc.returnToFirstCity();
        return vc;
    }

    private Path localSearch_2(final Path startPath) {
        final int MAX = 5;
        final int minIndex = 0;
        final int maxIndex = startPath.size() - 1;
        Path vc = startPath.copy();
        Path best = startPath.copy();
        double vcLength;
        double vnLength;
        boolean local = false;

        for (int t = 0; t < MAX; t++) {
            do {
                final int p1 = Tools.getRandomInt(minIndex, maxIndex);
                final int p2 = Tools.getRandomInt(minIndex, maxIndex);
                final Path vn = vc.copy();
                vn.swap(p1, p2);
                vcLength = vc.getPathLength();
                vnLength = vn.getPathLength();
                if (vnLength < vcLength)
                    vc = vn.copy();
                else
                    local = true;

            } while (!local);
            if (vcLength < best.getPathLength())
                best = vc.copy();
        }

        best.returnToFirstCity();
        return best;
    }

    private Path localSearch_2_fromAll(final Path startPath) {
        final int MAX = 5;
        final int maxIndex = startPath.size() - 1;
        Path vc = startPath.copy();
        Path best = startPath.copy();
        double vcLength = vc.getPathLength();
        double vnLength;

        for (int t = 0; t < MAX; t++) {
            for (int i = 0; i < maxIndex; i++) {
                vcLength = vc.getPathLength();
                // .out.println(i + ". Długość VC " + vcLength);
                for (int j = 1; j < maxIndex; j++) {
                    final Path vn = vc.copy();
                    vn.swap(i, j);
                    vnLength = vn.getPathLength();
                    // System.out.println("\t" + i + "." + j + " Długość VN " + vnLength);
                    if (vnLength < vcLength)
                        vc = vn.copy();
                }
            }
            if (vcLength < best.getPathLength())
                best = vc.copy();
        }

        best.returnToFirstCity();
        return best;
    }

    private void fillPath(final Path path) {
        for (final Point p : points) {
            path.add(p);
        }
    }

}
