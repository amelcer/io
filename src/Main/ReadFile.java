package Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFile {
    public static ArrayList<String> getDataFromFile(final String path) {
        try {
            final String absolutePath = new File("").getAbsolutePath();
            System.out.println(absolutePath.concat(path));
            final File file = new File(absolutePath.concat(path));
            final Scanner sc = new Scanner(file);
            final ArrayList<String> lines = new ArrayList<>();
            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }

            sc.close();
            return lines;
        } catch (FileNotFoundException fi) {
            System.out.println("File not found");
        }

        return new ArrayList<String>();
    }
}
