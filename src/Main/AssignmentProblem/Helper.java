package Main.AssignmentProblem;

import java.util.ArrayList;

public class Helper {
    public static int[][] getTasks(final int size, final ArrayList<String> lines) {
        final int[][] tasks = new int[size][size];
        int lineNumber = 1;
        for (int i = 0; i < size; i++) {
            final ArrayList<Integer> line = new ArrayList<>();
            do {
                final String[] tmp = lines.get(lineNumber).trim().split(" ");

                for (final String num : tmp)
                    line.add(Integer.parseInt(num));

                lineNumber++;
            } while (line.size() < size);

            for (int j = 0; j < size; j++) {
                tasks[i][j] = line.get(j);
            }

            line.clear();
        }

        return tasks;
    }

    public static int result(final int[][] tasksTime, final ArrayList<Integer> taskAssignedToWorker) {
        int sum = 0;
        final int size = tasksTime.length;
        for (int worker = 0; worker < size; worker++) {
            final int task = taskAssignedToWorker.get(worker);
            sum += tasksTime[worker][task];
        }

        return sum;
    }
}
