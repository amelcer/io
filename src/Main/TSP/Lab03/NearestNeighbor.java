package Main.TSP.Lab03;

import Main.TSP.Path;
import Main.TSP.Point;

public class NearestNeighbor {

    Point[] points;
    int dimension;

    public NearestNeighbor(Point[] points) {
        this.points = points;
        this.dimension = points.length;
    }

    Point getNearest(final Point point, Point[] points) {
        double lowestDistance = Double.MAX_VALUE;
        Point nearest = null;
        for (final Point p : points) {
            if (p.getId() == point.getId() || p.isVisited())
                continue;

            final double distance = point.getDistanceTo(p);
            if (distance < lowestDistance) {
                nearest = p;
                lowestDistance = distance;
            }
        }

        return nearest;
    }

    public Path findPath(Point startPoint) {
        Path path = new Path();

        Point currentPoint = startPoint;
        for (int i = 0; i < dimension - 1; i++) {
            path.add(currentPoint);
            currentPoint.visit();
            final Point nearest = getNearest(currentPoint, points);
            if (nearest != null)
                currentPoint = nearest;
        }

        path.add(points[0]);

        return path;
    }

}
