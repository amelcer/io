package Main.TSP;

import Main.Tools;
import Main.TSP.Lab03.NearestNeighbor;
import Main.TSP.Lab04.LocalSearch;

import java.io.FileNotFoundException;
import java.io.IOException;

public class TSP {

    static void printResults(final Path path, final String name) {
        System.out.println("########### Algorytm " + name + " ###########");
        path.printPath();
        System.out.println("Dystans: " + path.getPathLength());
    }

    private Point getRandomStartPoint(final Point[] points) {
        final int maxIndex = points.length - 1;
        final int minIndex = 0;
        final int index = Tools.getRandomInt(minIndex, maxIndex);
        return points[index];
    }

    static Path getRandomPath(Point[] points) {
        Path randomPath = new Path();
        for (Point point : points) {
            randomPath.add(point);
        }
        int size = randomPath.size();
        int maxIndex = size - 1;
        for (int i = 0; i < size; i++) {
            int point1 = Tools.getRandomInt(0, maxIndex);
            int point2 = Tools.getRandomInt(0, maxIndex);
            randomPath.swap(point1, point2);
        }

        return randomPath;
    }

    public static void main(final String[] args) {
        final Point[] points;
        try {
            points = Helper.getPointsFromFile("/src/Main/TSP/graph.txt");
        } catch (final FileNotFoundException e) {
            System.out.println(e.getMessage());
            return;
        }

        String name = "";
        try {
            final Path nearestNeighbor = new NearestNeighbor(points).findPath(points[0]);
            printResults(nearestNeighbor, "Najbliższego sąsiada");
            SaveFile.savePath(nearestNeighbor, "NajblizszySasiad.csv");

            final LocalSearch localSearch = new LocalSearch(points);

            name = "Lokalne przeszukiwanie 1.1 Pierwszy lepszy";
            final Path local1_1 = localSearch.findPath1_1();
            SaveFile.savePath(local1_1, "Local1_1_PierwszyLepszy.csv");
            printResults(local1_1, name);

            name = "Lokalne przeszukiwanie 1.1 Dla wszystkich";
            final Path local1_2 = localSearch.findPath1_2();
            SaveFile.savePath(local1_2, "Local1_1_WszyscySasiedzi.csv");
            printResults(local1_2, name);

            Path randomGeneratedPath = getRandomPath(points);
            randomGeneratedPath.returnToFirstCity();
            System.out.println("################## Długość losowo wygenerowanej trasy: "
                    + randomGeneratedPath.getPathLength() + " ##################");
            SaveFile.savePath(randomGeneratedPath, "LosowoWylosowanaSciezka.csv");
            randomGeneratedPath.removeLast();

            name = "Lokalne przeszukiwanie 1.2 Pierwszy lepszy";
            final Path local1_3 = localSearch.findPath1_3(randomGeneratedPath);
            SaveFile.savePath(local1_3, "Local1_2_PierwszyLepszy.csv");
            printResults(local1_3, name);

            name = "Lokalne przeszukiwanie 1.2 Dla wszystkich";
            final Path local1_4 = localSearch.findPath1_4(randomGeneratedPath);
            SaveFile.savePath(local1_4, "Local1_2_WszyscySasiedzi.csv");
            printResults(local1_4, name);

            name = "Lokalne przeszukiwanie 1.3 Pierwszy lepszy";
            final Path local1_5 = localSearch.findPath1_5(nearestNeighbor);
            SaveFile.savePath(local1_5, "Local1_3_PierwszyLepszy.csv");
            printResults(local1_5, name);

            name = "Lokalne przeszukiwanie 1.3 Dla wszystkich";
            final Path local1_6 = localSearch.findPath1_6(nearestNeighbor);
            SaveFile.savePath(local1_6, "Local1_3_WszyscySasiedzi.csv");
            printResults(local1_6, name);

            // ###############
            // LOCAL SEARCH 2
            // ###############

            name = "Lokalne przeszukiwanie 2.1 Pierwszy lepszy";
            final Path local2_1 = localSearch.findPath2_1();
            SaveFile.savePath(local2_1, "Local2_1_PierwszyLepszy.csv");
            printResults(local2_1, name);

            name = "Lokalne przeszukiwanie 2.1 Dla wszystkich";
            final Path local2_2 = localSearch.findPath2_2();
            SaveFile.savePath(local2_2, "Local1_1_WszyscySasiedzi.csv");
            printResults(local2_2, name);

            name = "Lokalne przeszukiwanie 2.2 Pierwszy lepszy";
            final Path local2_3 = localSearch.findPath2_3(randomGeneratedPath);
            SaveFile.savePath(local2_3, "Local2_2_PierwszyLepszy.csv");
            printResults(local2_3, name);

            name = "Lokalne przeszukiwanie 2.2 Dla wszystkich";
            final Path local2_4 = localSearch.findPath2_4(randomGeneratedPath);
            SaveFile.savePath(local2_4, "Local2_2_WszyscySasiedzi.csv");
            printResults(local2_4, name);

            name = "Lokalne przeszukiwanie 2.3 Pierwszy lepszy";
            final Path local2_5 = localSearch.findPath2_5(nearestNeighbor);
            SaveFile.savePath(local2_5, "Local2_3_PierwszyLepszy.csv");
            printResults(local2_5, name);

            name = "Lokalne przeszukiwanie 2.3 Dla wszystkich";
            final Path local2_6 = localSearch.findPath2_6(nearestNeighbor);
            SaveFile.savePath(local2_6, "Local2_3_WszyscySasiedzi.csv");
            printResults(local2_6, name);

        } catch (final Error | IOException e) {
            System.out.println(e.getMessage() + "dla algorytmu " + name);
        }

    }
}
