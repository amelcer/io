const fileUploader = document.querySelector("#fileUpload")
const canvas = document.querySelector("canvas")
const ctx = canvas.getContext("2d")
ctx.font = "24px serif"
const CANVAS_PADDING = 100
const MARGIN_TOP = 50
let maxX
let maxY
let path

fileUploader.addEventListener("change", (e) => {
    const input = e.target
    const reader = new FileReader()
    reader.readAsBinaryString(input.files[0])
    reader.onloadend = () => {
        path = parseCSV(reader.result)
        maxX = Math.max(...path.map((p) => p.x))
        maxY = Math.max(...path.map((p) => p.y))
        resizeCanvas()
        drawGraph(50)
    }
})

const parseCSV = (data) => {
    const csv = data.split("\n").filter((r) => r !== "")
    return csv.map((row) => {
        const [id, x, y] = row.split(";")
        if (id && x && y)
            return {
                id,
                x: Number(x),
                y: Number(y),
            }
    })
}

const wait = (time) => new Promise((resolve) => setTimeout(resolve, time))

const drawGraph = async (sleep = 0) => {
    for (const { x, y, id } of path) {
        ctx.fillText(id, x, y + MARGIN_TOP)
        ctx.fillStyle = "blue"
        ctx.fill()
    }

    ctx.strokeStyle = "red"
    for (const { x, y } of path) {
        ctx.quadraticCurveTo(x, y + MARGIN_TOP, x, y + MARGIN_TOP)
        //await wait(sleep)
        ctx.stroke()
    }
}

const resizeCanvas = async (callback = null) => {
    const CANVAS_WIDTH = document.querySelector(".canvas-container").clientWidth - 50
    const CANVAS_HEIGHT = document.querySelector(".canvas-container").clientHeight - 50
    canvas.width = CANVAS_WIDTH
    canvas.height = CANVAS_HEIGHT

    const scaleX = canvas.width / (maxX + CANVAS_PADDING)
    const scaleY = canvas.height / (maxY + CANVAS_PADDING)

    ctx.scale(scaleX, scaleY)
    if (callback && path) callback()
}

window.addEventListener("resize", () => resizeCanvas(() => drawGraph(0)))
resizeCanvas()
