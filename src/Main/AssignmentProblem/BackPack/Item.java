package Main.AssignmentProblem.BackPack;

public class Item {
    private int id;
    private double weight;
    private double size;

    public Item(int id, double weight, double size) {
        this.id = id;
        this.setWeight(weight);
        this.setSize(size);
    }

    public int getId() {
        return id;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        Item obj = (Item) o;
        return weight == obj.getWeight() && size == obj.getSize() && id == obj.getId();
    }

    public String toString() {
        return "ID " + getId() + " Wartość: " + getWeight() + " Wielkość:" + getSize();
    }
}
