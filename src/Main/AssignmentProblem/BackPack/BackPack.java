package Main.AssignmentProblem.BackPack;

import java.util.ArrayList;

public class BackPack {
    ArrayList<Item> backpack = new ArrayList<>();
    private int backpackSize;

    public BackPack(int size) {
        backpackSize = size;
    }

    public int getBackpackSize() {
        return backpackSize;
    }

    public double getSizeOfItems() {
        int size = 0;
        for (Item item : backpack) {
            size += item.getSize();
        }
        return size;
    }

    public double getWeightOfItems() {
        int weight = 0;
        for (Item item : backpack) {
            weight += item.getWeight();
        }
        return weight;
    }

    public double getFreeSpace() {
        return backpackSize - getSizeOfItems();
    }

    public boolean itemFit(Item item) {
        double currentSize = getSizeOfItems();
        if (currentSize + item.getSize() < backpackSize) {
            return true;
        }

        return false;
    }

    public void addItem(Item item) {
        if (itemFit(item) && !backpack.contains(item))
            backpack.add(item);
    }

    public Item getItem(int index) {
        return backpack.get(index);
    }

    public void swap(int position, Item newItem) {
        backpack.set(position, newItem);
    }

    public boolean canSwap(int position, Item item) {
        if (backpack.contains(item))
            return false;

        Item currentItem = backpack.get(position);
        double spaceAfterChange = getFreeSpace() + currentItem.getSize() - item.getSize();

        return spaceAfterChange >= 0;
    }

    public BackPack clone() {
        BackPack newBackpack = new BackPack(backpackSize);
        newBackpack.backpack = (ArrayList<Item>) this.backpack.clone();

        return newBackpack;
    }

    public int size() {
        return backpack.size();
    }

    public boolean contains(Item item) {
        return backpack.contains(item);
    }

    public String toString() {
        String items = "";
        for (int i = 0; i < backpack.size(); i++) {
            Item item = backpack.get(i);
            items += (i + 1) + "." + item.toString() + " \n";
        }

        return items + " Wolne miejsce:" + getFreeSpace() + " \n Wartość: " + getWeightOfItems();
    }

}
