package Main.AssignmentProblem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

import Main.ReadFile;
import Main.Tools;

public class TabuSearch {

    static private final int TABU_LENGTH = 5000;
    static private final int CADENCE = 5000;
    final private static int MAX_ITERATIONS = 2000;
    final private static int MAX_TRIES = 20;
    static private final Queue<Move> tabu = new LinkedList<>();

    static class Move {
        int worker1;
        int worker2;
        int job1;
        int job2;

        public Move(int worker1, int job1, int worker2, int job2) {
            this.worker1 = worker1;
            this.worker2 = worker2;
            this.job1 = job1;
            this.job2 = job2;
        }

        @Override
        public boolean equals(Object o) {
            Move obj = (Move) o;
            return worker1 == obj.worker1 && worker2 == obj.worker2 && job1 == obj.job1 && job2 == obj.job2;
        }
    }

    static void addMove(Move move) {
        int tabuSize = tabu.size();
        if (tabuSize < CADENCE) {
            tabu.add(move);
            return;
        }

        tabu.poll();
        tabu.add(move);
    }

    static boolean isInTabu(Move move) {
        return tabu.contains(move);
    }

    private static void swap(final ArrayList<Integer> assignedTasks) {
        final int size = assignedTasks.size() - 1;
        final int worker1 = Tools.getRandomInt(0, size);
        final int worker2 = Tools.getRandomInt(0, size);
        final int job1 = assignedTasks.get(worker1);
        final int job2 = assignedTasks.get(worker2);

        Move tmp = new Move(worker1, job1, worker2, job2);
        if (!isInTabu(tmp)) {
            assignedTasks.set(worker1, job2);
            assignedTasks.set(worker2, job1);
            addMove(tmp);
        }

    }

    private static int tabuSearch(final int[][] tasks, ArrayList<Integer> taskAssignedToWorker) {
        tabu.clear();
        int vc = Helper.result(tasks, taskAssignedToWorker);

        for (int i = 0; i < MAX_ITERATIONS; i++) {
            for (int j = 0; j < MAX_TRIES; j++) {
                final ArrayList<Integer> vnTasks = (ArrayList<Integer>) taskAssignedToWorker.clone();
                swap(vnTasks);
                final int vn = Helper.result(tasks, vnTasks);

                if (vn < vc) {
                    vc = vn;
                    taskAssignedToWorker = (ArrayList<Integer>) vnTasks.clone();
                    break;
                }
            }
        }

        return vc;
    }

    private static int getMaxTimeTask(int[][] tasks, int worker, ArrayList<Integer> taskAssignedToWorker) {
        int max = tasks[worker][0];
        int maxIndex = 0;
        for (int i = 0; i < tasks[worker].length; i++) {
            if (tasks[worker][i] > max && !taskAssignedToWorker.contains(i)) {
                max = tasks[worker][i];
                maxIndex = i;
            }
        }

        return maxIndex;
    }

    public static void main(final String[] args) {
        final ArrayList<String> lines = ReadFile.getDataFromFile("/src/Main/AssignmentProblem/assign100.txt");
        final int workers = Integer.parseInt(lines.get(0).trim().split(" ")[0]);
        final int[][] tasks = Helper.getTasks(workers, lines);
        ArrayList<Integer> taskAssignedToWorker = new ArrayList<>();
        int size = tasks.length;
        for (int i = 0; i < size; i++) {
            // int task = getMaxTimeTask(tasks, i, taskAssignedToWorker);
            // taskAssignedToWorker.add(task);
            taskAssignedToWorker.add(i);
            Collections.shuffle(taskAssignedToWorker);
        }
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            final int sa = tabuSearch(tasks, (ArrayList<Integer>) taskAssignedToWorker.clone());
            System.out.println(sa);
            // System.out.println(i + ". Próba " + sa);
            sum += sa;
        }

        System.out.println("Średnia " + sum / 10);

    }
}
