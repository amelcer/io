package Main.TSP;

import Main.ReadFile;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Helper {

    public static Point[] getPointsFromFile(String filePath) throws FileNotFoundException {
        final ArrayList<String> lines = ReadFile.getDataFromFile(filePath);
        if (lines.size() == 0) {
            throw new FileNotFoundException("Brak odczytanych danych");
        }

        int dimension = Integer.parseInt(lines.get(3).split(":")[1].trim());
        Point[] points = new Point[dimension];

        int index = 0;
        for (int i = 6; i < lines.size(); i++) {
            final String[] line = lines.get(i).split(" ");
            final int id = Integer.parseInt(line[0]);
            final double x = Double.parseDouble(line[1]);
            final double y = Double.parseDouble(line[2]);
            points[index] = new Point(id, x, y);
            index++;
        }

        return points;
    }

}
