package Main.Lab02;

import Main.ReadFile;

import java.util.ArrayList;
import java.util.Arrays;

public class Binpack {
    static int binpackSize, amountOfItems, besSolution;
    static int[] items;

    static void print(final int[] arr) {
        for (final int i : arr) {
            System.out.print(i + ", ");
        }
        System.out.println();
    }

    static int nextFit() {
        int binPack = 0;
        final int[] bins = new int[amountOfItems];

        for (int i = 0; i < amountOfItems; i++) {
            if (bins[binPack] + items[i] > binpackSize) {
                binPack++;
            }
            bins[binPack] += items[i];
        }

        return binPack + 1;
    }

    static int firstFit() {
        final int[] bins = new int[amountOfItems];

        for (int i = 0; i < amountOfItems; i++) {
            for (int j = 0; j < amountOfItems; j++) {
                if (bins[j] + items[i] <= binpackSize) {
                    bins[j] += items[i];
                    break;
                }
            }
        }
        System.out.print("FirstFit: ");
        print(bins);
        return Arrays.stream(bins).filter(it -> it > 0).toArray().length;
    }

    static int bestFit() {
        int[] bins = new int[amountOfItems];

        for (int i = 0; i < amountOfItems; i++) {
            bins = reverse(Arrays.stream(bins).sorted().toArray());
            for (int j = 0; j < amountOfItems; j++) {
                if (bins[j] + items[i] <= binpackSize) {
                    bins[j] += items[i];
                    break;
                }
            }

        }

        System.out.print("BestFit: ");
        print(bins);
        return Arrays.stream(bins).filter(it -> it > 0).toArray().length;
    }

    static int worstFit() {
        final ArrayList<Integer> bins = new ArrayList<>();
        boolean added;
        bins.add(items[0]);
        for (int i = 1; i < amountOfItems; i++) {
            bins.sort(Integer::compare);
            added = false;
            for (int j = 0; j < bins.size(); j++) {
                if (bins.get(j) + items[i] <= binpackSize) {
                    bins.set(j, bins.get(j) + items[i]);
                    added = true;
                    break;
                }
            }

            if (!added) {
                bins.add(items[i]);
            }
        }

        System.out.print("WorstFit: ");
        bins.forEach(b -> System.out.print(b + ", "));
        System.out.println();
        return bins.size();
    }

    static int firstFitDecreasing() {
        final int[] depressingItems = reverse(Arrays.stream(items).sorted().toArray());
        final int[] bins = new int[amountOfItems];

        for (int i = 0; i < amountOfItems; i++) {
            for (int j = 0; j < amountOfItems; j++) {
                if (bins[j] + depressingItems[i] <= binpackSize) {
                    bins[j] += depressingItems[i];
                    break;
                }
            }
        }
        System.out.print("FirstFit: ");
        print(bins);
        return Arrays.stream(bins).filter(it -> it > 0).toArray().length;
    }

    static int[] reverse(final int[] arr) {
        final int[] reversed = new int[arr.length];
        int last = arr.length - 1;
        for (int i = 0; i < reversed.length; i++) {
            reversed[i] = arr[last];
            last--;
        }

        return reversed;
    }

    public static void main(final String[] args) {
        final ArrayList<String> lines = ReadFile.getDataFromFile("/src/Main/Lab02/binpack1.txt");
        if (lines.size() == 0) {
            System.out.println("Brak odczytanych linii");
            return;
        }

        final String[] firstLine = lines.get(0).split(" ");
        binpackSize = Integer.parseInt(firstLine[0]);
        amountOfItems = Integer.parseInt(firstLine[1]);
        besSolution = Integer.parseInt(firstLine[2]);

        items = new int[amountOfItems];

        for (int i = 1; i < lines.size(); i++) {
            items[i - 1] = Integer.parseInt(lines.get(i));
        }

        //NF 64
        System.out.println("NextFit, zajętych pojemników: " + nextFit());
        //FF 50
        System.out.println("FirstFit, zajętych pojemników: " + firstFit());
        //BF 50
        System.out.println("BestFit, zajętych pojemników: " + bestFit());
        //WF 56
        System.out.println("WorstFit, zajętych pojemników: " + worstFit());
        //FFD 49
        System.out.println("FirstFitDecreasing, zajętych pojemników: " + firstFitDecreasing());
    }
}
