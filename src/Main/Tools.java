package Main;

import java.util.Random;

public class Tools {

    public static int getRandomInt(final int min, final int max) {
        final Random rand = new Random();
        return rand.nextInt(max + 1 - min) + min;
    }

    public static double getRandomDouble() {
        final Random rand = new Random();
        return rand.nextDouble();
    }

    private static void print(final int[][] arr) {
        final int size = arr.length;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void print(final String[] arr) {
        final int size = arr.length;
        for (int i = 0; i < size; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
