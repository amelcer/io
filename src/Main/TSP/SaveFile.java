package Main.TSP;

import java.io.FileWriter;
import java.io.IOException;

public class SaveFile {

    public static void savePath(Path path, String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        writer.write("id;x;y \n");
        for (Point p : path) {
            String row = String.join(";", p.toString().split(" ")) + "\n";
            writer.write(row);
        }

        writer.write("\n\n");
        writer.write("pathLegnth \n");
        writer.write(String.valueOf(path.getPathLength()));
        writer.close();
    }
}
