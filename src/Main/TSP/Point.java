package Main.TSP;

public class Point {
    private final double x;
    private final double y;
    private final int id;
    private boolean visited;

    public Point(final int id, final double x, final double y) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.visited = false;
    }

    public int getId() {
        return this.id;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public boolean isVisited() {
        return this.visited;
    }

    public void visit() {
        this.visited = true;
    }

    public String toString() {
        return this.id + " " + this.x + " " + this.y;
    }

    public double getDistanceTo(final Point p2) {
        return Math.sqrt(Math.pow(p2.getX() - this.getX(), 2) + Math.pow(p2.getY() - this.getY(), 2));
    }
}
