package Main.AssignmentProblem.BackPack;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

import Main.ReadFile;
import Main.Tools;

public class TabuSearchBackPack {
    static private final int CADENCE = 50;
    final private static int MAX_ITERATIONS = 10000;
    static private final Queue<Move> tabu = new LinkedList<>();

    private static class Move {
        Item item1;
        Item item2;

        public Move(Item item1, Item item2) {
            this.item1 = item1;
            this.item2 = item2;
        }

        @Override
        public boolean equals(Object obj) {
            Move moveToCompare = (Move) obj;

            return this.item1.equals(moveToCompare.item1) && this.item2.equals(moveToCompare.item2);
        }

    }

    static void addMove(Move move) {
        int tabuSize = tabu.size();
        if (tabuSize < CADENCE) {
            tabu.add(move);
            return;
        }

        tabu.poll();
        tabu.add(move);
    }

    static boolean isInTabu(Move move) {
        return tabu.contains(move);
    }

    static ArrayList<Item> getItems(ArrayList<String> lines) {
        ArrayList<Item> items = new ArrayList<>();
        int n = lines.size();
        for (int i = 1; i < n; i++) {
            String[] line = lines.get(i).trim().split(" ");
            double weight = Double.parseDouble(line[0]);
            double size = Double.parseDouble(line[1]);
            items.add(new Item(i, weight, size));
        }

        return items;
    }

    static void fillBackPack(ArrayList<Item> items, BackPack backpack) {
        Collections.shuffle(items);
        items.forEach(i -> backpack.addItem(i));
    }

    static void fillBackPack2(ArrayList<Item> items, BackPack backpack) {
        items.sort((i1, i2) -> {
            double i1Value = i1.getWeight() / i1.getSize();
            double i2Value = i2.getWeight() / i2.getSize();

            return Double.compare(i1Value, i2Value);
        });
        items.forEach(i -> backpack.addItem(i));
    }

    private static void swap(BackPack backpack, ArrayList<Item> items) {
        final int size = backpack.size() - 1;
        int position1;
        int position2;
        do {
            position1 = Tools.getRandomInt(0, size);
            position2 = Tools.getRandomInt(0, items.size() - 1);
        } while (!backpack.canSwap(position1, items.get(position2)));

        Move move = new Move(backpack.getItem(position1), items.get(position2));
        if (!isInTabu(move)) {
            backpack.swap(position1, items.get(position2));
            addMove(move);
        }
    }

    static BackPack tabuSearch(ArrayList<Item> items, BackPack backpack) {
        tabu.clear();
        double vc = backpack.getWeightOfItems();
        BackPack best = backpack.clone();
        for (int i = 0; i < MAX_ITERATIONS; i++) {
            BackPack tmp = best.clone();
            swap(tmp, items);
            double vn = tmp.getWeightOfItems();
            if (vc < vn) {
                vc = vn;
                best = tmp.clone();
            }
        }

        return best;
    }

    private static void writeLines(ArrayList<String> lines, String fileName) {
        try {
            final String absolutePath = new File("").getAbsolutePath();
            File file = new File(absolutePath.concat(fileName));
            if (!file.exists())
                file.createNewFile();

            System.out.println(file.getPath());
            FileWriter writer = new FileWriter(file, false);
            for (String line : lines) {
                writer.write(line + "\n");
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Problem przy zapisie");
            e.printStackTrace();
        }
    }

    private static void writeToFile(String inputFileName, double[] sums, double[] relativeError,
            String outputFileName) {
        String filePath = "/src/Main/AssignmentProblem/BackPack/" + outputFileName;
        DecimalFormat formatter = new DecimalFormat("#.###");

        ArrayList<String> lines = ReadFile.getDataFromFile(filePath);
        if (lines.size() == 0) {
            lines.add(inputFileName + ";");
            for (int i = 0; i < sums.length; i++) {
                lines.add(formatter.format(sums[i]) + ";");
            }
            lines.add("RelativeError;");
            for (int i = 0; i < relativeError.length; i++) {
                lines.add(formatter.format(relativeError[i]) + ";");
            }

            writeLines(lines, "/src/Main/AssignmentProblem/BackPack/" + outputFileName);
            return;
        }

        ArrayList<String> updatedLines = new ArrayList<>();
        String firstLine = lines.get(0) + inputFileName + ";";
        updatedLines.add(firstLine);
        int nextLine = 1;
        for (int i = 0; i < sums.length; i++) {
            String line = lines.get(nextLine) + formatter.format(sums[i]) + ";";
            updatedLines.add(line);
            nextLine++;
        }

        String RelativeErrorLine = lines.get(nextLine) + "RelativeError;";
        updatedLines.add(RelativeErrorLine);
        nextLine++;

        for (int i = 0; i < relativeError.length; i++) {
            String line = lines.get(nextLine) + formatter.format(relativeError[i]) + ";";
            updatedLines.add(line);
            nextLine++;
        }

        writeLines(updatedLines, "/src/Main/AssignmentProblem/BackPack/" + outputFileName);
    }

    public static void main(String[] args) {
        int tries = 10;
        // small instances
        // String outputFileName = "output_small_instances_2.csv";
        // String inputFileDirectory = "small_instances/";
        // String[] files = { "f1_l-d_kp_10_269", "f2_l-d_kp_20_878", "f3_l-d_kp_4_20",
        // "f4_l-d_kp_4_11",
        // "f5_l-d_kp_15_375",
        // "f6_l-d_kp_10_60", "f7_l-d_kp_7_50", "f8_l-d_kp_23_10000", "f9_l-d_kp_5_80",
        // "f10_l-d_kp_20_879" };
        // double[] optimums = { 295, 1024, 35, 23, 481.0694, 52, 107, 9767, 130, 1025
        // };

        // large instances
        String outputFileName = "output_large_instances_2.csv";
        String inputFileDirectory = "large_instances/";
        String[] files = { "knapPI_1_100_1000_1", "knapPI_1_200_1000_1", "knapPI_1_500_1000_1", "knapPI_1_1000_1000_1",
                "knapPI_1_2000_1000_1", "knapPI_1_5000_1000_1", "knapPI_1_10000_1000_1" };
        double[] optimums = { 9147, 11238, 28857, 54503, 110625, 276457, 563647 };

        final String absolutePath = new File("").getAbsolutePath();
        File file = new File(absolutePath.concat("/src/Main/AssignmentProblem/BackPack/" + outputFileName));
        if (file.exists())
            file.delete();

        for (int i = 0; i < files.length; i++) {
            String filePath = "/src/Main/AssignmentProblem/BackPack/" +
                    inputFileDirectory + files[i];
            final ArrayList<String> lines = ReadFile
                    .getDataFromFile(filePath);

            final int backpackSize = Integer.parseInt(lines.get(0).trim().split(" ")[1]);
            final ArrayList<Item> items = getItems(lines);

            double avgSum = 0;
            double avgRelativeErrorSum = 0;
            double[] sums = new double[tries + 1];
            double[] relativeError = new double[tries + 1];
            double optimum = optimums[i];

            for (int j = 0; j < tries; j++) {
                BackPack backpack = new BackPack(backpackSize);
                // fillBackPack(items, backpack);
                fillBackPack2(items, backpack);

                BackPack optimized = tabuSearch(items, backpack);
                double x = optimized.getWeightOfItems();
                avgSum += x;
                sums[j] = x;
                relativeError[j] = (Math.abs(optimum - x) / optimum);
                avgRelativeErrorSum += relativeError[j];
            }

            sums[tries] = avgSum / tries;
            relativeError[tries] = avgRelativeErrorSum / tries;

            writeToFile(files[i], sums, relativeError, outputFileName);
        }

    }

}
